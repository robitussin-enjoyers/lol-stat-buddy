import React from 'react'
import AppBar from './Components/AppBar'
import { Routes,Route } from "react-router";
import './App.css'
import Champions from './Components/Pages/Champions';
import Items from './Components/Pages/Items'
import Home from './Components/Pages/Home'

const App = () => {
  return (
    <div className="container">
      <AppBar className='header'/>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/Champions" element={<Champions />} />
        <Route path="/Items" element={<Items />} />
      </Routes>
      <h3 className='footer'>Byeeee...</h3>
    </div>
  );
}

export default App;
