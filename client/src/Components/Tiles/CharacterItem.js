import React from 'react'

const CharacterItem = ({ item }) => {
  return (
    <div className='card'>
        <img src={item.img} alt='' />
          <div className='info'>
          <ul>
            <li>
              <strong>{item.name}</strong>
            </li>
            <li>
              <strong>Role:</strong> {item.roles}
            </li>
            <li>
              <strong>Win Rate:</strong> {item.win_rate}
            </li>
            <li>
              <strong>Pick Rate:</strong> {item.pick_rate}
            </li>
            <li>
            <strong>Ban Rate:</strong> {item.ban_rate}
          </li>
        </ul>
      </div>
    </div>
  )
}

export default CharacterItem