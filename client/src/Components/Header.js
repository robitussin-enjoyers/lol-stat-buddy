import React from 'react'
import AppBar from './AppBar'

const Header = () => {
  return (
    <header className='header'>
      <AppBar></AppBar>
    </header>
  )
}

export default Header