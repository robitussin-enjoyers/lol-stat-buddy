import React, {useState, useEffect} from 'react'
//Uncomment for material UI searchbar
import Search from '../Search-new'
// import Search from '../Search'
import Logo from "../../logo-temp-large.png"

const Home = () => {

  const [items, setItems] = useState([])
  const [query, setQuery] = useState('')

  useEffect(() => {
    const searching = async () => {
      var champs = await fetch('http://localhost:5000/champions', {
        method:'GET',
        headers: {
          'Content-Type': 'application/json'
        },
      }).then((response) => response.json())
      var items = await fetch('http://localhost:5000/items', {
        method:'GET',
        headers: {
          'Content-Type': 'application/json'
        },
      }).then((response) => response.json())
      var con = items.concat(champs)
      let nameOnly = con.map(o => o.name)
      let data = [...new Set(nameOnly)];

      data.forEach((o, i) => o.id = i + 1);
      return data
    }
    searching().then((data) => {
      console.log(data)
      setItems(data);
    })
  }, [query])


  //Remove query function to test MUI Searchbar
  // const queryFunction = (q) => {
  //   setQuery(q)
  // }
  return (
    <div className = 'container'>
        <h1 className='content'>
            <p><img src={Logo} alt="Logo :)"/></p>
             <Search query = {query} items={items}/>
             <span>  </span>
             <span>  </span>
        </h1>
    </div>

  )
}

export default Home