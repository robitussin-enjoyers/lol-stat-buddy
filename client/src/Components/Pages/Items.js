import React, {useState, useEffect} from 'react'
import Search from '../Search'
import CharacterGrid from '../Tiles/CharacterGrid'
import axios from 'axios'

const Items = () => {

    const [items, setItems] = useState([])
    const [isLoading, setIsLoading] = useState(true)
    const [query, setQuery] = useState('')
  
    useEffect(() => {
      const fetchItems = async() => {
        const result = await axios(
          `https://www.breakingbadapi.com/api/characters?name=${query}`
        )
  
        console.log(result.data)
        setItems(result.data)
        setIsLoading(false)
      }
      fetchItems();
    }, [query])

  return (
    <div className ='container'>
        <h1 className='content'>
            {/* <p><img src={Logo} alt="Logo :)"/></p> */}
            <Search getQuery = {(q) => setQuery(q)} items={items}/>
            <CharacterGrid isLoading = {isLoading} items={items}/>
        </h1>
    </div>
  )
}

export default Items