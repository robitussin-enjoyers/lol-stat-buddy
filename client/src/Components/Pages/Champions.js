import React, {useState, useEffect} from 'react'
import Search from '../Search'
import CharacterGrid from '../Tiles/CharacterGrid'


const Champions = () => {

    const [items, setItems] = useState([])
    const [isLoading, setIsLoading] = useState(true)
    const [query, setQuery] = useState('')

    useEffect(() => {
      const searching = async () => {
        const res = await fetch('http://localhost:5000/champions', {
          method:'GET',
          headers: {
            'Content-Type': 'application/json'
          }
        })
        const data = await res.json()
        return data
      }
      searching().then((data) => {
        console.log(data)
        const matches = data.filter(s => s.name.toLowerCase().match(query))
        // const unique = [...new Set(suggestions)];
        // const suggestionObjects = unique.map((str) => {
        // const strReplace = str.replace(/[^a-zA-Z0-9]/g, '');
        // let imgStr = strReplace
        //   if (imgStr == "Wukong") {
        //     imgStr = 'MonkeyKing'
        //   }
        //   if (imgStr == "NunuWillump") {
        //     imgStr = "Nunu"
        //   }
        //   return {name:str, img:`http://localhost:5000/champion/icon/${imgStr}`}
        // });
        console.log(matches)
        setItems(matches);
      })
      setIsLoading(false)
    }, [query])

  const queryFunction = (q) =>{
    setQuery(q)
  }

  return (
    <div className = 'body'>
        <h1 className='content'>
            {/* <p><img src={Logo} alt="Logo :)"/></p> */}
            <Search getQuery = {queryFunction} items={items.name}/>
            <CharacterGrid isLoading = {isLoading} items={items}/>
        </h1>
    </div>

  )
}

export default Champions