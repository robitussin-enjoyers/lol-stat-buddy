import PropTypes from 'prop-types'

const Button = ({ color, text, onClick , classy }) => {
  return (
    <button
      onClick={onClick}
      style={{ backgroundColor: color }}
      className= {classy}
    >
      {text}
    </button>
  )
}

Button.defaultProps = {
  color: 'rgb(11, 90, 155)',
  calssy: 'btn'
}

Button.propTypes = {
  text: PropTypes.string,
  color: PropTypes.string,
  onClick: PropTypes.func,
}

export default Button