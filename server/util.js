import {get_champions, get_items, get_matches, update_matches, update_champion_stats, get_champion_stats} from './db/db-if.js';
import {getSummonerByName, getSummonerMatchHistory, getMatchSummary} from './riot/riot-if.js';
import Match from './db/models/match.js'

function getLatestDragon(){
    // This function is going to sniff out the latest champion/item data,unzip it,and return the good stuff
}

const getSuggestionsForStr = async (baseString) => {
    // Query the db for champions and items. should probably have a cache for this...
    // @TODO: Use a trie!
    const championList = await get_champions().then((champions) => {
        return champions.map((champ) => champ.name)
    })

    const itemList = await  get_items().then((items) => {
        return items.map((item) => item.name)
    })

    console.log(itemList)
    const champs = championList.filter(x => x.includes(baseString))
    const items = itemList.filter(x => x.includes(baseString))
    
    const champsJson = JSON.stringify(champs);
    const itemsJson = JSON.stringify(items);

    var json = "{\"champion_suggestions\": "+champsJson+", \"item_suggestions\": "+itemsJson+", \"summoner_suggestions\":[]}"

    return json
}

const getImageNameForChamp = async (id) => {
    const champions = await get_champions({key: id})
    return champions[0].image.full;
}

const getImageNameForItem = async (id) => {
    const items = await get_items({id: id})
    return items[0]?.image?.full;
}

const getChampionsAndStats = async () => {
    return get_champion_stats();
}

const getItemsAndStats = async () => {
    const json = await get_items()
    const items = Object.values(json)
    const stats = items.map((item) => { 
        return { name: item.name, 
                 id: item.id,
                 roles: ['Bene Gesserit'],
                 pick_rate: 1,
                 ban_rate: 0.05 };
    })
    return stats;
}

const sleep = ms => new Promise(r => setTimeout(r, ms));
const uploadMatchDataToDb = async (summoner) => {
    var summonerDetails = await getSummonerByName(summoner)
    var summonerPuuid = summonerDetails['puuid']
    var summonerQueue = []
    var visitedSummoners = []
    var matchIdToSummaryMap = {  };
    summonerQueue.push(summonerPuuid);
    while(summonerQueue.length > 0 && Object.keys(matchIdToSummaryMap).length < 5000){
        try{
            console.log('here '+summonerQueue.length+' '+Object.keys(matchIdToSummaryMap).length)
            var puuid = summonerQueue.shift();
            visitedSummoners.push(puuid)
            var matchIds = await getSummonerMatchHistory(puuid, 20)
            for(let i = 0; i < matchIds.length; i++){
                var matchId = matchIds[i]
                if(matchIdToSummaryMap[matchId] === undefined){
                    var matchSummary = await getMatchSummary(matchId)
                    matchIdToSummaryMap[matchId] = matchSummary
                    var puuids = matchSummary['metadata']['participants']
                    puuids.forEach(x => {
                        if(!visitedSummoners.includes(x)){
                            summonerQueue.push(x)
                        }
                    })
                }
            }
        }
        catch{

        }

        await sleep(10000);
    }
    console.log(Object.keys(matchIdToSummaryMap))
    console.log(summonerQueue.length)
    console.log(visitedSummoners)
    update_matches(Object.values(matchIdToSummaryMap))
    

    // Grab the summoner puuid from db
    // Get all matches from db into memory
    // bfs:
        // Get match history of previous 100 matches for puuid
        // set aside any matches that already exist in memory
        // Get all unique summoners from match history
        // bfs on each unique summoner
    // stop when number of matches is 10000 or so to limit db size
        
}

const updateChampionStats = async () => {
    const json = await get_champions()
    const champions = Object.values(json)
    const matches = await get_matches()
    const champ_match_stats = compute_champion_match_stats(champions.map(champ => {return champ.key}), matches)
    const possible_roles = ["TOP", "MIDDLE", "JUNGLE", "CARRY", "SUPPORT"]
    const stats = champions.map((champion) => { 
        var champ_stats = champ_match_stats.get(champion.key)
        var pick = champ_stats.pick
        var win = champ_stats.win
        var roles = []
        possible_roles.forEach(role => {
            const times_picked_role = champ_stats.roles[role]
            if(times_picked_role > 0 && times_picked_role / champ_stats.pick > 0.2){
                roles.push(role)
            }
        })
        return { name: champion.name, 
                 id: champion.key,
                 roles: roles,
                 win_rate: pick == 0 ? 0 : win / pick,
                 pick_rate: pick / matches.length,
                 ban_rate: 0.25 };
    })
    update_champion_stats(stats)
}

const compute_champion_match_stats = function (champion_id_list, matches){
    const champ_map = new Map();
    champion_id_list.forEach(champId => {
            champ_map.set(champId, {
                win:0,
                pick:0,
                ban:0,
                roles:{"TOP": 0, "MIDDLE": 0, "JUNGLE": 0, "CARRY": 0, "SUPPORT": 0}
            })
        }
    )
    matches.forEach(match => {
        match.info.participants.forEach(participant => {
            var champId = participant.championId
            var champ = champ_map.get(champId)
            champ.pick++
            if(participant.win)
                champ.win++
            if(participant.lane == "TOP")
                champ.roles["TOP"]++
            if(participant.lane == "MIDDLE")
                champ.roles["MIDDLE"]++
            if(participant.lane == "JUNGLE")
                champ.roles["JUNGLE"]++
            if(participant.lane == "BOTTOM")
            {
                if(participant.role == "CARRY")
                    champ.roles["CARRY"]++
                if(participant.role == "SUPPORT")
                    champ.roles["SUPPORT"]++
            }
        })
    })
    return champ_map
}

export { getSuggestionsForStr, getChampionsAndStats, getItemsAndStats, getImageNameForChamp, getImageNameForItem, uploadMatchDataToDb, updateChampionStats };