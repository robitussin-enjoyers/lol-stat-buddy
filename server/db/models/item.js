import mongoose from 'mongoose'
const Schema = mongoose.Schema

const itemRuneSchema = new Schema({
    isrune: {
        type: Boolean,
        required: false
    },
    tier: {
        type: Number,
        required: false
    },
    type: {
        type: String,
        required: false
    }
})

const itemGoldSchema = new Schema({
    base: {
        type: Number,
        required: false
    },
    total: {
        type: Number,
        required: false
    },
    sell: {
        type: Number,
        required: false
    },
    purchasable: {
        type: Boolean,
        required: false
    }
})

const itemStatsSchema = new Schema({

    FlatHPPoolMod: {
        type: Number,
        required: false
    },
    rFlatHPModPerLevel: {
        type: Number,
        required: false
    },
    FlatMPPoolMod: {
        type: Number,
        required: false
    },
    rFlatMPModPerLevel: {
        type: Number,
        required: false
    },
    PercentHPPoolMod: {
        type: Number,
        required: false
    },
    PercentMPPoolMod: {
        type: Number,
        required: false
    },
    FlatHPRegenMod: {
        type: Number,
        required: false
    },

    rFlatHPRegenModPerLevel: {
        type: Number,
        required: false
    },
    FlatMPRegenMod: {
        type: Number,
        required: false
    },
    rFlatMPRegenModPerLevel: {
        type: Number,
        required: false
    },
    PercentMPRegenMod: {
        type: Number,
        required: false
    },
    FlatArmorMod: {
        type: Number,
        required: false
    },
    rFlatArmorModPerLevel: {
        type: Number,
        required: false
    },
    PercentArmorMod: {
        type: Number,
        required: false
    },
    rFlatArmorPenetrationMod:{
        type: Number,
        required: false
    },
    rFlatArmorPenetrationModPerLevel:{
        type: Number,
        required: false
    },
    rPercentArmorPenetrationMod:{
        type: Number,
        required: false
    },
    rPercentArmorPenetrationModPerLevel:{
        type: Number,
        required: false
    },
    FlatPhysicalDamageMod:{
        type: Number,
        required: false
    },
    rFlatPhysicalDamageModPerLevel:{
        type: Number,
        required: false
    },
    PercentPhysicalDamageMod:{
        type: Number,
        required: false
    },
    FlatMagicDamageMod:{
        type: Number,
        required: false
    },
    rFlatMagicDamageModPerLevel:{
        type: Number,
        required: false
    },
    PercentMagicDamageMod:{
        type: Number,
        required: false
    },
    FlatMovementSpeedMod:{
        type: Number,
        required: false
    },
    rFlatMovementSpeedModPerLevel:{
        type: Number,
        required: false
    },
    PercentMovementSpeedMod:{
        type: Number,
        required: false
    },
    rPercentMovementSpeedModPerLevel:{
        type: Number,
        required: false
    },
    FlatAttackSpeedMod:{
        type: Number,
        required: false
    },
    PercentAttackSpeedMod:{
        type: Number,
        required: false
    },
    rPercentAttackSpeedModPerLevel:{
        type: Number,
        required: false
    },
    rFlatDodgeMod:{
        type: Number,
        required: false
    },
    rFlatDodgeModPerLevel:{
        type: Number,
        required: false
    },
    PercentDodgeMod:{
        type: Number,
        required: false
    },
    FlatCritChanceMod:{
        type: Number,
        required: false
    },
    rFlatCritChanceModPerLevel:{
        type: Number,
        required: false
    },
    PercentCritChanceMod:{
        type: Number,
        required: false
    },
    FlatCritDamageMod:{
        type: Number,
        required: false
    },
    rFlatCritDamageModPerLevel:{
        type: Number,
        required: false
    },
    PercentCritDamageMod:{
        type: Number,
        required: false
    },
    FlatBlockMod:{
        type: Number,
        required: false
    },
    PercentBlockMod:{
        type: Number,
        required: false
    },
    FlatSpellBlockMod:{
        type: Number,
        required: false
    },
    rFlatSpellBlockModPerLevel:{
        type: Number,
        required: false
    },
    PercentSpellBlockMod:{
        type: Number,
        required: false
    },
    FlatEXPBonus:{
        type: Number,
        required: false
    },
    PercentEXPBonus:{
        type: Number,
        required: false
    },
    rPercentCooldownMod:{
        type: Number,
        required: false
    },
    rPercentCooldownModPerLevel:{
        type: Number,
        required: false
    },
    rFlatTimeDeadMod:{
        type: Number,
        required: false
    },
    rFlatTimeDeadModPerLevel:{
        type: Number,
        required: false
    },
    rPercentTimeDeadMod:{
        type: Number,
        required: false
    },
    rPercentTimeDeadModPerLevel:{
        type: Number,
        required: false
    },
    rFlatGoldPer10Mod:{
        type: Number,
        required: false
    },
    rFlatMagicPenetrationMod:{
        type: Number,
        required: false
    },
    rFlatMagicPenetrationModPerLevel:{
        type: Number,
        required: false
    },
    rPercentMagicPenetrationMod:{
        type: Number,
        required: false
    },
    rPercentMagicPenetrationModPerLevel:{
        type: Number,
        required: false
    },
    FlatEnergyRegenMod:{
        type: Number,
        required: false
    },
    rFlatEnergyRegenModPerLevel:{
        type: Number,
        required: false
    },
    FlatEnergyPoolMod:{
        type: Number,
        required: false
    },
    rFlatEnergyModPerLevel:{
        type: Number,
        required: false
    },
    PercentLifeStealMod:{
        type: Number,
        required: false
    },
    PercentSpellVampMod:{
        type: Number,
        required: false
    }

})

const itemImgSchema = new Schema({
    full: {
        type: String, 
        required: false
    },
    sprite: {
        type: String, 
        required: false
    },
    group: {
        type: String, 
        required: false
    },
    x: {
        type: Number, 
        required: false
    },
    y: {
        type: Number, 
        required: false
    },
    w: {
        type: Number, 
        required: false
    },
    h: {
        type: Number, 
        required: false
    }
})

const itemSchema = new Schema({
    id: {
        type: Number, 
        required: true
    },
    name: {
        type: String,
        required: false
    },
    image: {
        type: itemImgSchema,
        required: false
    },
    rune: {
        type: itemRuneSchema,
        required: false
    },
    gold: {
        type: itemGoldSchema,
        required: false
    },
    group: {
        type: String,
        required: false
    },
    description: {
        type: String,
        required: false
    },
    colloq: {
        type: String,
        required: false
    },
    plaintext: {
        type: String,
        required: false
    },
    consumed: {
        type: Boolean,
        required: false
    },
    stacks: {
        type: Number,
        required: false
    },
    depth: {
        type: Number,
        required: false
    },
    consumeOnFull: {
        type: Boolean,
        required: false
    },
    from: {
        type: [String],
        required: false
    },
    into: {
        type: [String],
        required: false
    },
    specialRecipe : {
        type: Number,
        required: false
    },
    inStore: {
        type: Boolean,
        required: false
    },
    hideFromAll: {
        type: Boolean,
        required: false
    },
    requiredChampion: {
        type: String,
        required: false
    },
    requiredAlly: {
        type: String,
        required: false
    },
    stats: {
        type: itemStatsSchema,
        required: false
    },
    tags: {
        type: [String],
        required: false
    },
    maps: {
        type: Map,
        of: Boolean,
        required: false
    }
})

const Item = mongoose.model("Item", itemSchema)
export default Item