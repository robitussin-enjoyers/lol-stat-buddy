import {truncate} from 'fs/promises'
import mongoose from 'mongoose'

const Schema = mongoose.Schema

const championInfoSchema = new Schema({
    attack: {
        type: Number,
        required: false
    },
    defense: {
        type: Number,
        required: false
    },
    magic: {
        type: Number,
        required: false
    },
    difficulty: {
        type: Number,
        required: false
    }
})

const championImgSchema = new Schema({
    full: {
        type: String,
        required: false
    },
    sprite: {
        type: String,
        required: false
    },
    group: {
        type: String,
        required: false
    },
    x: {
        type: Number,
        required: false
    },
    y: {
        type: Number,
        required: false
    },
    w: {
        type: Number,
        required: false
    },
    h: {
        type: Number,
        required: false
    }
})

const championStatsSchema = new Schema({
    hp: {
        type: Number,
        required: false
    },
    hpperlevel: {
        type: Number,
        required: false
    },
    mp: {
        type: Number,
        required: false
    },
    mpperlevel: {
        type: Number,
        required: false
    },
    movespeed: {
        type: Number,
        required: false
    },
    armor: {
        type: Number,
        required: false
    },
    armorperlevel: {
        type: Number,
        required: false
    },
    spellblock: {
        type: Number,
        required: false
    },
    spellblockperlevel: {
        type: Number,
        required: false
    },
    attackrange: {
        type: Number,
        required: false
    },
    hpregen: {
        type: Number,
        required: false
    },
    hpregenperlevel: {
        type: Number,
        required: false
    },
    mpregen: {
        type: Number,
        required: false
    },
    mpregenperlevel: {
        type: Number,
        required: false
    },
    crit: {
        type: Number,
        required: false
    },
    critperlevel: {
        type: Number,
        required: false
    },
    attackdamage: {
        type: Number,
        required: false
    },
    attackdamageperlevel: {
        type: Number,
        required: false
    },
    attackspeedperlevel: {
        type: Number,
        required: false
    },
    attackspeed: {
        type: Number,
        required: false
    }
}) 

const championSchema = new Schema({
    version: {
        type: String,
        required: false
    },
    id: {
        type: String,
        required: true
    },
    key: {
        type: String,
        required: true
    },
    name: {
        type: String,
        required: true
    },
    title: {
        type: String,
        required: false
    },
    blurb: {
        type: String,
        required: true
    },
    info: {
        type: championInfoSchema,
        required: false
    },
    image: {
        type: championImgSchema,
        required: false
    },
    tags: {
        type: [String],
        required: false
    },
    partype: {
        type: String,
        required: false
    },
    stats: {
        type: championStatsSchema,
        required: true
    }
})

const Champion = mongoose.model("Champion", championSchema)
export default Champion