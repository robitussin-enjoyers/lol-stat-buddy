import mongoose from 'mongoose'
const Schema = mongoose.Schema

const participantSchema = new Schema({
    championName: {
        type: String,
        required: true
    },
    championId: {
        type: String,
        required: true
    },
    item0: {
        type: Number,
        required: false
    },
    item1: {
        type: Number,
        required: false
    },
    item2: {
        type: Number,
        required: false
    },
    item3: {
        type: Number,
        required: false
    },
    item4: {
        type: Number,
        required: false
    },
    item5: {
        type: Number,
        required: false
    },
    item6: {
        type: Number,
        required: false
    },
    lane: {
        type: String,
        required: true
    },
    role: {
        type: String,
        required: true
    },
    win: {
        type: Boolean,
        required: true
    }
})

const infoSchema = new Schema({
    gameMode: {
        type: String,
        required: true
    },
    participants: {
        type: [participantSchema],
        required: true
    }
})

const metadataSchema = new Schema({
    participants: {
        type: [String],
        required: true
    },
    matchId: {
        type: String,
        required: true
    }
})

const matchSchema = new Schema({
    metadata: {
        type: metadataSchema,
        required: true
    },
    info: {
        type: infoSchema,
        required: true
    }
})

const Match = mongoose.model("Match", matchSchema)
export default Match