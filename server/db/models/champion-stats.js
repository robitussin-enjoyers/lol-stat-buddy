import {truncate} from 'fs/promises'
import mongoose from 'mongoose'

const Schema = mongoose.Schema

const championStatsSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    id: {
        type: Number,
        required: true
    },
    win_rate: {
        type: Number,
        required: true
    },
    pick_rate: {
        type: Number,
        required: true
    },
    ban_rate: {
        type: Number,
        required: false
    },
    roles: {
        type: [String],
        required: false
    }
})

const ChampionStats = mongoose.model("ChampionStats", championStatsSchema)
export default ChampionStats