import pg from 'pg'
const ClientClass = pg.Client
const pgUrl = "postgres://mnirggwe:I3FuGHRWiUzJ7jKFDSvb_hiVNrzU6hFv@jelani.db.elephantsql.com/mnirggwe"
const client = new ClientClass(pgUrl)
let connected = false

async function run_query(func){
    try{
        connect_db()
        return await func()
    }
    catch(ex){
        console.log('Some error '+ex)
    }
}

async function connect_db(){
    if(!connected){
        await client.connect()
        console.log('Client connected.')
        connected = true
    }
}

async function disconnect_db(){
    await client.end()
    connected = false
}

async function update_champions(championList){
    await run_query(async () => {
        await client.query('DROP TABLE IF EXISTS champions;')
        await client.query('CREATE TABLE champions (id serial primary key, name text);')
        
        championList = await championList.map((x) => {
            return x.replace("'", "''")
        })

        let championQuery = await "('" + championList.join("'), ('") + "')";
        let fullQuery = await 'INSERT INTO champions (name) VALUES '+championQuery+';'
        await client.query(fullQuery)
    })
}

async function get_champions(){
    let retval = await run_query(async () => {
        const {rows} = await client.query('SELECT name FROM champions;')
        return rows
    })
    let championNames = retval.map(x => x.name)
    return championNames
}

async function update_items(itemList){
    await run_query(async () => {
        await client.query('DROP TABLE IF EXISTS items;')
        await client.query('CREATE TABLE items (id primary key, name text);')
        itemList = await itemList.map((x) => {
            return x.replaceAll("'", "''")
        })

        let itemQuery = await "('" + itemList.join("'), ('") + "')";
        let fullQuery = await 'INSERT INTO items (id, name) VALUES '+itemQuery+';'

        await client.query(fullQuery)
    })
}

function parenthisize(obj)
{
    
}

async function get_items(){
    let retval = await run_query(async () => {
        const {rows} = await client.query('SELECT name FROM items;')
        return rows
    })
    let itemNames = retval.map(x => x.name)
    return itemNames
}

export { connect_db, disconnect_db, update_champions, get_champions, update_items, get_items };