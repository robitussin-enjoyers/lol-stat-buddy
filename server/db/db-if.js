import pg from 'pg'
import mongoose from 'mongoose'
const dbUrl = "mongodb+srv://merle:V1ZK1iqgYN603j1C@dice-mania.32sl9.mongodb.net/?retryWrites=true&w=majority"
import Champion from './models/champion.js'
import Item from './models/item.js'
import Match from './models/match.js'
import ChampionStats from './models/champion-stats.js'
let connected = false

async function run_query(func){
    try{
        return await func()
    }
    catch(ex){
        console.log('Some error '+ex)
    }
}

function connect_db(){
    if(!connected){
        console.log('Client connecting...')
        mongoose.connect(dbUrl, { useNewUrlParser: true, useUnifiedTopology: true } )
        console.log('Client connected.')
        connected = true
    }
    else{
        console.log('Already connected.')
    }
}

function disconnect_db(){
    mongoose.connection.close()
    connected = false
}

function update_champions(championDragonData){
    run_query(() => {
        Champion.deleteMany()
        Champion.create(championDragonData)
    })
}

async function get_champions(filter={}){
    return await run_query(async () => {
        return Champion.find(filter)
    })
}

function update_items(itemDragonData){
    run_query(() => {
        Item.deleteMany()
        Item.create(itemDragonData)
    })
}

async function get_items(filter={}){
    return await run_query(async () => {
        return Item.find(filter)
    })
}

function update_matches(matchData){
    run_query(() => {
        Match.deleteMany()
        Match.create(matchData)
    })
}

async function get_matches(filter={}){
    return await run_query(async () => {
        return Match.find(filter)
    })
}

function update_champion_stats(championStats){
    run_query(() => {
        ChampionStats.deleteMany()
        ChampionStats.create(championStats)
    })
}

async function get_champion_stats(filter={}){
    return await run_query(async () => {
        return ChampionStats.find(filter)
    })
}

export { connect_db, disconnect_db, update_champions, get_champions, update_items, get_items, update_matches, get_matches, update_champion_stats, get_champion_stats };