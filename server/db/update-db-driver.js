import {extractChampionsFromDragon, extractItemsFromDragon} from '../riot/dragon-if.js';
import {connect_db, disconnect_db, update_champions, get_champions, update_items, get_items} from './db-if.js';

await connect_db()


async function updateDragon(){
    console.log("getting champs from dragon")
    let champions = extractChampionsFromDragon()
    console.log("updating champs")
    await update_champions(champions)
    console.log("getting items from dragon")
    let items = extractItemsFromDragon()
    console.log("updating items")
    await update_items(items)
    console.log("done")
}