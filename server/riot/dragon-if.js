import fs, {readdirSync} from 'fs'
import https from 'https'
import jsZip from 'jszip'

function extractChampionsFromDragon(){
    let championJsonFile = getUsDataPath()+'champion.json'
    var championsString = fs.readFileSync(championJsonFile, (err, data) => {
        if(err){
            console.error(err);
            return;
        }
    })  

    var json = JSON.parse(championsString).data
    var champs = Object.values(json)
    return champs
}

function extractItemsFromDragon(){
    let itemJsonFile = getUsDataPath()+'item.json'
    var itemsString = fs.readFileSync(itemJsonFile, 'utf8', (err, data) => {
        if(err){
            console.error(err);
            return;
        }
    })  
    var jsonMap = JSON.parse(itemsString).data
    var items = []
    for(var key in jsonMap)
    {
        var value = jsonMap[key]
        value.id = key
        items.push(value)
    }
    return items
}
/*
function getChampionNames(){
    let championJson = extractChampionsFromDragon()
    let names = []
    for (var championKey in championJson["data"]){
        names.push(championJson["data"][championKey]["name"])
    }

    return names.sort()
}

function getItemNames(){
    let itemJson = extractItemsFromDragon()
    let names = []
    for (var itemKey in itemJson["data"]){
        names.push(itemJson["data"][itemKey]["name"])
    }

    return names.sort()
}*/

function getUsDataPath(){
    var dir = 'riot/dragon-data/'

    const getDirectories = source =>
      readdirSync(source, { withFileTypes: true })
        .filter(dirent => dirent.isDirectory())
        .map(dirent => dirent.name)

    // The patch directory is the first one alpha-numerically because it is numeric
    const patchDir = getDirectories(dir)[0]
    dir += patchDir+'/data/en_US/'
    return dir
}

function getLatestDragonData(){
    // @TODO: Need to get the latest, not a static version, by checking https://ddragon.leagueoflegends.com/api/versions.json to see what patches exist
    var url = 'https://ddragon.leagueoflegends.com/cdn/dragontail-10.10.5.zip'

    // Download the data to zip file
    https.get(url, (res) => {
        var result_file_name = 'dragon-data.zip'
        const stream = fs.createWriteStream(result_file_name);
        res.pipe(stream);
        //res.pipe(unzip.Extract({ path: './dragon.zip' }))

        stream.on('finish', () => {
            stream.close();
            console.log('Dragon Data Downloaded');
        });
        /* to help when we need to unzip a tarball
        if (EXT_NAME === '.zip') {
            res.pipe(unzip.Extract({ path: './tensorflow' }));
           } {
            res.pipe(gunzip()).pipe(tar.extract('./tensorflow'));
           }
           */
        
        jsZip.loadAsync(result_file_name).then(function (zip) {
            Object.keys(zip.files).forEach(function (filename) {
            zip.files[filename].async('string').then(function (fileData) {
                console.log(fileData) // These are your file contents      
            })
            })
        })
    }).on('error', (err) => {
        throw 'Failed to download latest Dragon data!';
    });
}

export { extractChampionsFromDragon, extractItemsFromDragon, getLatestDragonData/*, getChampionNames, getItemNames*/ };