// riot api key RGAPI-853f4fd7-ce4d-4b09-a152-99888896a958

// the list of available patches https://ddragon.leagueoflegends.com/api/versions.json
// need to query data dragon with the correct patch. returns a tarball or a zip

//function accountDetailsByKey 
//riot/account/v1/accounts/me

import fetch from 'node-fetch';

const baseEndpoint = 'https://na1.api.riotgames.com/lol'
const apiKey = 'RGAPI-621d5116-e82d-46a9-a570-eb230d2de144'

async function getFreeChampionRotation(){
    var url = baseEndpoint+'/platform/v3/champion-rotations'
    return getConvertStream(url)
}

function getSummonerForUuid(){

}

async function getSummonerByName(name){
  var url = baseEndpoint+'/summoner/v4/summoners/by-name/'+name
  return getConvertStream(url)
}

async function getSummonerMatchHistory(puuid, count){
  var url = 'https://americas.api.riotgames.com/lol/match/v5/matches/by-puuid/'+puuid+'/ids?start=0&count='+count
  var matches = getConvertStream(url)
  return matches
}

async function getMatchSummary(matchId){
  var url = 'https://americas.api.riotgames.com/lol/match/v5/matches/'+matchId
  var match = getConvertStream(url)
  return match
}

async function getConvertStream(url)
{
  const response = await fetch(url, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      "X-Riot-Token": apiKey
    }
  })
  return streamToString(response.body).then((body) => { return JSON.parse(body) } );
}

function streamToString (stream) {
  const chunks = [];
  return new Promise((resolve, reject) => {
    stream.on('data', (chunk) => chunks.push(Buffer.from(chunk)));
    stream.on('error', (err) => reject(err));
    stream.on('end', () => resolve(Buffer.concat(chunks).toString('utf8')));
  })
}

export { getFreeChampionRotation, getSummonerByName, getSummonerMatchHistory, getMatchSummary };
