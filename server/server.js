import express from 'express'
import cors from 'cors'
import {getSuggestionsForStr, getChampionsAndStats, getItemsAndStats, getImageNameForChamp, getImageNameForItem} from './util.js'
import {connect_db, disconnect_db} from "./db/db-if.js"

const app = express();
const port = process.env.PORT || 5000;

app.use(cors());
app.use(express.json());

app.listen(port, () => {
    console.log(`Server is running on port: ${port}`);
    connect_db()
});

app.post('/search-suggestion', async (req, res) => {

    var search_str = await req.body.search_str;
    var suggestions = await getSuggestionsForStr(search_str);
    await res.send(suggestions);
})

app.get('/champions', async (req, res) => {
    var champsAndStats = await getChampionsAndStats();
    await res.send(champsAndStats);
})

app.get('/items', async (req, res) => {
    var itemsAndStats = await getItemsAndStats();
    await res.send(itemsAndStats);
})

app.get('/champion/icon/:champion_id', async (req, res) => {
    getImageNameForChamp(req.params.champion_id).then((fileName) => {
        res.sendFile('/resource/images/champions/'+fileName, { root: '.' });
    })
})

app.get('/item/icon/:item_id', async (req, res) => {
    getImageNameForItem(req.params.item_id).then((fileName) => {
        res.sendFile('/resource/images/items/'+fileName, { root: '.' });
    })
})