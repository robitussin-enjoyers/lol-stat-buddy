import {getFreeChampionRotation, getSummonerByName, getSummonerMatchHistory} from './riot/riot-if.js';
import {uploadMatchDataToDb, updateChampionStats} from './util.js'
import { getChampionsAndStats } from './util.js';
import {connect_db} from './db/db-if.js'
connect_db()
//var results = await uploadMatchDataToDb('WillemDafoesPony')
//var champs_and_stats = await getChampionsAndStats()
console.log("updating champion stats")
await updateChampionStats()
console.log("champion stats updated")